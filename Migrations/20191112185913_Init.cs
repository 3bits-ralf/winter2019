﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace server.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    GameId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.GameId);
                });

            migrationBuilder.CreateTable(
                name: "Highscores",
                columns: table => new
                {
                    HighscoreId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Points = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Highscores", x => x.HighscoreId);
                });

            migrationBuilder.CreateTable(
                name: "Stack",
                columns: table => new
                {
                    StackId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Items = table.Column<int>(nullable: false),
                    GameId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stack", x => x.StackId);
                    table.ForeignKey(
                        name: "FK_Stack_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "GameId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Players",
                columns: table => new
                {
                    Alias = table.Column<string>(nullable: false),
                    Token = table.Column<string>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    CurrentGameGameId = table.Column<int>(nullable: true),
                    HighscoreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Players", x => x.Alias);
                    table.ForeignKey(
                        name: "FK_Players_Games_CurrentGameGameId",
                        column: x => x.CurrentGameGameId,
                        principalTable: "Games",
                        principalColumn: "GameId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Players_Highscores_HighscoreId",
                        column: x => x.HighscoreId,
                        principalTable: "Highscores",
                        principalColumn: "HighscoreId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Players_CurrentGameGameId",
                table: "Players",
                column: "CurrentGameGameId");

            migrationBuilder.CreateIndex(
                name: "IX_Players_HighscoreId",
                table: "Players",
                column: "HighscoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Stack_GameId",
                table: "Stack",
                column: "GameId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Players");

            migrationBuilder.DropTable(
                name: "Stack");

            migrationBuilder.DropTable(
                name: "Highscores");

            migrationBuilder.DropTable(
                name: "Games");
        }
    }
}
