using Microsoft.EntityFrameworkCore;

namespace Server
{
    public class WinterDbContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Game> Games { get; set; }

        public DbSet<Highscore> Highscores { get; set; }

        //public DbSet<Log> Log { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=winter.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*
                modelBuilder.Entity<Player>()
                    .HasOne(p => p.CurrentGame);

                modelBuilder.Entity<Player>()
                    .HasOne(p => p.Highscore);

                modelBuilder.Entity<Game>()
                    .HasMany(g => g.Stacks);

                modelBuilder.Entity<Game>()
                    .HasNoKey();

                modelBuilder.Entity<Stack>()
                    .HasNoKey();

                modelBuilder.Entity<Highscore>()
                    .HasNoKey();
            */
        }
    }
}
