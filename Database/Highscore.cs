using System.ComponentModel.DataAnnotations;

namespace Server
{
    public class Highscore
    {
        public int HighscoreId { get; set; }

        public string Points { get; set; }
    }
}
