using System.ComponentModel.DataAnnotations;

namespace Server
{
    public class Stack
    {
        public int StackId { get; set; }

        public int Items { get; set; }
    }
}
