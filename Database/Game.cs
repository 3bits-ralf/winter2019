using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Server
{
    public class Game
    {
        public int GameId { get; set; }

        public ICollection<Stack> Stacks { get; set; }

        public Game()
        {
            this.Stacks = new List<Stack>();
        }

	public int NimSum
	{
	    get { return this.Stacks.Aggregate(0, (acc, curr) => acc ^ curr.Items); }
	}
	
	public override string ToString()
	{
	    return string.Join(" ", this.Stacks.OrderBy(s => s.StackId).Select(s => s.Items)) + " Nim sum = " + this.NimSum; 
	}
    }
}
