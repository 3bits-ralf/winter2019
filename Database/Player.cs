using System.ComponentModel.DataAnnotations;

namespace Server
{
    public class Player
    {
        [Key]
        public string Alias { get; set; }
        public string Token { get; set; }

        public int Level { get; set; }

        public Game CurrentGame { get; set; }
        public Highscore Highscore { get; set; }

	public override string ToString()
	{
	    return this.Alias;
	}
    }
}
