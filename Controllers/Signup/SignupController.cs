﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Mime;
using Microsoft.AspNetCore.Http;
using Server.WebData;
using Microsoft.EntityFrameworkCore;

namespace Server.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class SignupController : ControllerBase
    {
        private ILogger<SignupController> Logger { get; }

        public SignupController(
            ILogger<SignupController> logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Sign up a new player.
        /// </summary>
        /// <param name="alias">The new player's name.</param>
        /// <returns>A secure player identification token.</returns>
	[Route("/Signup")]
	[HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<PlayerToken> Signup(string alias)
        {
            using (var dbContext = new WinterDbContext())
            {
                var player = dbContext.Players.FirstOrDefault(p => p.Alias == alias);

                if (player == null)
                {
                    player = new Player
                    {
                        Alias = alias,
                        Level = 1,
                    };

                    dbContext.Players.Add(player);
                }

                string newToken;
                int i = 0;
                for (; ; )
                {
                    newToken = $"{this.CreateNewToken(alias)}-{i++}";
                    if (dbContext.Players.Any(p => p.Token == newToken))
                        continue;

                    break;
                }

                player.Token = newToken;

                dbContext.SaveChanges();

                return Ok(new PlayerToken
                {
                    Token = player.Token,
                });
            }
        }

        /// <summary>
        /// Reset your player score and history. NOTE: DISABLED UNTIL SUSPECTED BUG HAS BEEN INVESTIGATED!
        /// </summary>
        /// <param name="token">The secure player identification token.</param>
        /// <returns>The secure player identification token.</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<PlayerToken> ResetPlayer(PlayerToken token)
        {
	    return BadRequest($"Could not find player with token '{token.Token}'.");
	
            using (var dbContext = new WinterDbContext())
            {
                var player = dbContext.Players
                    .Include(p => p.CurrentGame)
		    .Include(p => p.Highscore)
                    .FirstOrDefault(p => p.Token == token.Token);

                if (player == null)
                    return BadRequest($"Could not find player with token '{token.Token}'.");

		player.Level = 0;
                player.CurrentGame = null;
		player.Highscore = null;
                dbContext.SaveChanges();

		    return Ok(token);
            }
        }


        private string CreateNewToken(string alias)
        {
            return $"{alias}-{DateTime.Now.ToString("yyyyMMddHHmm")}";
        }
    }
}
