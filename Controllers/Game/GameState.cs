using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.Json.Serialization;

namespace Server.WebData
{
    /// <summary>
    /// Describes the state of the game.
    ///
    /// When returned via an API, it is always your turn to play.
    ///
    /// When all stacks contain 0 cubes, the game is over.
    /// </summary>
    public class GameState
    {
        /// <summary>
        /// A stack of cubes.
        /// </summary>
        public class Stack
        {
            /// <summary>
            /// Amount of cubes in this stack.
            /// </summary>
            public int Cubes { get; set; }
        }

        /// <summary>
        /// The game status.
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public enum GameStatus
        {
            Unknown,
            Playing,
            GameOver,
        }

        /// <summary>
        /// The game status.
        /// </summary>
        public GameStatus Status { get; set; }

        /// <summary>
        /// Describes the stacks.
        /// </summary>
        public List<Stack> Stacks { get; set; }

        internal static GameState FromDbGame(Game currentGame)
        {
            var gameState = new GameState
            {
                Status = currentGame.Stacks.Sum(s => s.Items) > 0 ? GameStatus.Playing : GameStatus.GameOver,
                Stacks = currentGame.Stacks.OrderBy(s => s.StackId).Select(s => new Stack { Cubes = s.Items }).ToList(),
            };

            return gameState;
        }
    }
}
