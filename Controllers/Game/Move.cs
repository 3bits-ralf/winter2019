using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Server.WebData
{
    /// <summary>
    /// Descibes the move you want to make.
    /// </summary>
    public class Move
    {
        /// <summary>
        /// The secure player identification token.
        /// </summary>
        /// <value>A securely generated token.</value>
        public string PlayerToken { get; set; }
        
        /// <summary>
        /// Describes which stack you want to remove cubes from.
        /// 
        /// The first stack is 1. The second = 2 and so on. This is a 1-based index.
        /// </summary>
        /// <value>The which stack.</value>
        public int WhichStack { get; set; }
        
        /// <summary>
        /// Descibes how many cubes you want to remove from the stack.
        /// </summary>
        /// <value>The number of cubes to remove.</value>
        public int NumberToRemove { get; set; }
    }
}
