using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Mime;
using Microsoft.AspNetCore.Http;
using Server.WebData;
using Microsoft.EntityFrameworkCore;
using System.Numerics;
using Serilog;
using System.Collections.Concurrent;
using System.Threading;

namespace Server.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class GameController : ControllerBase
    {
        private static readonly Random Random = new Random();
        private static ConcurrentDictionary<string, object> Gates = new ConcurrentDictionary<string, object>();

        private ILogger<GameController> Logger { get; }
        private Serilog.ILogger MoveLogger { get; }


        public GameController(
            ILogger<GameController> logger)
        {
            this.Logger = logger;

            this.MoveLogger = Log.ForContext(Serilog.Core.Constants.SourceContextPropertyName, "Moves");
        }

        /// <summary>
        /// Peek at the current state of the game without making a move.
        /// </summary>
        /// <param name="token">The secure player identification token.</param>
        /// <returns>The current state of the game. It is your turn to play.</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<GameState> Peek(PlayerToken token)
        {
            using (var dbContext = new WinterDbContext())
            {
                var player = dbContext.Players
                    .Include(p => p.CurrentGame)
                    .ThenInclude(g => g.Stacks)
                    .FirstOrDefault(p => p.Token == token.Token);

                if (player == null)
                    return BadRequest($"Could not find player with token '{token.Token}'.");

                if (player.CurrentGame == null)
                    return BadRequest($"No game running for player '{player.Alias}'.");

                if (player.CurrentGame.Stacks.Sum(s => s.Items) == 0)
                    return BadRequest($"Game is over for player '{player.Alias}'. Please start a new game.");

                return Ok(GameState.FromDbGame(player.CurrentGame));
            }
        }

        private object GetEnsuredGate(string token)
        {
            return Gates.GetOrAdd(token, _ => new object());
        }

        /// <summary>
        /// Starts a new game.
        /// </summary>
        /// <param name="token">The secure player identification token.</param>
        /// <returns>The current state of the game. It is your turn to play.</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<GameState> StartNew(PlayerToken token)
        {
            using (var dbContext = new WinterDbContext())
            {
                var player = dbContext.Players.FirstOrDefault(p => p.Token == token.Token);

                if (player == null)
                    return BadRequest($"Could not find player with token '{token.Token}'.");

                if (player.Level >= 127)
                    Monitor.Enter(this.GetEnsuredGate(token.Token));

                try
                {
                    if (player.Level >= 128)
                        player = dbContext.Players.FirstOrDefault(p => p.Token == token.Token);

                    player.CurrentGame = this.CreateNewGame(player);

                    dbContext.SaveChanges();

                    this.MoveLogger.Information("{gameId}: Player {player} started new game: {game}.", player.CurrentGame.GameId, player, player.CurrentGame);

                    return Ok(GameState.FromDbGame(player.CurrentGame));
                }
                finally
                {
                    if (player.Level >= 127)
                        Monitor.Exit(this.GetEnsuredGate(token.Token));
                }
            }
        }

        private Game CreateNewGame(Player player)
        {
            var playerLevel = player.Level;
            if (playerLevel <= 0)
                playerLevel = 1;

            var numberOfStacks = 2 + (int)(playerLevel / 3);
            var maxItemsInStacks = 10 + (int)Math.Log(playerLevel * 10);
            var minItemsInStacks = 3;

            var game = new Game();

            for (int i = 0; i < numberOfStacks; i += 1)
            {
                game.Stacks
                    .Add(new Stack
                    {
                        Items = Random.Next(minItemsInStacks, maxItemsInStacks + 1),
                    });
            }

            if (player.Level >= 40)
            {
                var nim_sum = game.Stacks.Aggregate(0, (acc, curr) => acc ^ curr.Items);

                if (nim_sum != 0)
                {
                    foreach (var stack in game.Stacks)
                    {
                        var target_size = stack.Items ^ nim_sum;
                        if (target_size < stack.Items)
                        {
                            var amount_to_remove = stack.Items - target_size;

                            stack.Items -= amount_to_remove;
                            break;
                        }
                    }
                }
            }

            return game;
        }

        /// <summary>
        /// Make your move!
        /// </summary>
        /// <param name="move">Describes the move you want to make.</param>
        /// <returns>The current state of the game. It is your turn to play.</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<GameState> Move(Move move)
        {
            using (var dbContext = new WinterDbContext())
            {
                var player = dbContext.Players
                    .Include(p => p.CurrentGame)
                    .ThenInclude(g => g.Stacks)
                    .Include(p => p.Highscore)
                    .FirstOrDefault(p => p.Token == move.PlayerToken);

                if (player == null)
                    return BadRequest($"Could not find player with token '{move.PlayerToken}'.");

                if (player.Level >= 127)
                    Monitor.Enter(this.GetEnsuredGate(move.PlayerToken));

                try
                {
                    if (player.Level >= 128)
                        player = dbContext.Players
                           .Include(p => p.CurrentGame)
                           .ThenInclude(g => g.Stacks)
                           .Include(p => p.Highscore)
                           .FirstOrDefault(p => p.Token == move.PlayerToken);

                    if (player.CurrentGame.Stacks.Sum(s => s.Items) == 0)
                        return BadRequest($"Game is over. Please start a new game.");

                    if (move.WhichStack < 1 || move.WhichStack > player.CurrentGame.Stacks.Count())
                        return BadRequest($"No such stack. Values accepted for this game: from 1 to {player.CurrentGame.Stacks.Count()}.");

                    if (move.NumberToRemove < 1)
                        return BadRequest($"You must remove at least one cube.");

                    var playerStack = player.CurrentGame.Stacks.OrderBy(s => s.StackId).ElementAt(move.WhichStack - 1);

                    if (playerStack.Items < move.NumberToRemove)
                        return BadRequest($"The stack is not large enough. The number of cubes in stack {move.WhichStack} is {player.CurrentGame.Stacks.ElementAt(move.WhichStack - 1).Items}.");

                    playerStack.Items -= move.NumberToRemove;

                    this.MoveLogger.Information("{gameId}: Player {player} removed {numberToRemove} items from stack {whichStack}. Game after move: {game}.", player.CurrentGame.GameId, player, move.NumberToRemove, move.WhichStack, player.CurrentGame);

                    if (player.CurrentGame.Stacks.Sum(s => s.Items) != 0)
                    {
                        this.ComputerMakeMove(player, player.CurrentGame);

                        if (player.CurrentGame.Stacks.Sum(s => s.Items) == 0)
                        {
                            this.MoveLogger.Information("{gameId}: Player {player} won game.", player.CurrentGame.GameId, player);

                            if (player.Highscore == null)
                            {
                                player.Highscore = new Highscore();
                            }

                            BigInteger score;
                            if (!BigInteger.TryParse(player.Highscore.Points, out score))
                            {
                                score = 0;
                            }

                            score += 1;

                            player.Highscore.Points = score.ToString();
                            player.Level = (int)Math.Floor(0.5 + Math.Sqrt(1.0 + 8.0 * ((double)score) / (5.0)) / 2.0);
                        }
                    }

                    dbContext.SaveChanges();

                    this.MoveLogger.Information("{gameId}: Game after move. Saved as {game}.", player.CurrentGame.GameId, player.CurrentGame);

                    return Ok(GameState.FromDbGame(player.CurrentGame));
                }
                finally
                {
                    if (player.Level >= 127)
                        Monitor.Exit(this.GetEnsuredGate(move.PlayerToken));
                }
            }
        }

        private void MakeDumbMove(Game game)
        {
            var index = game.Stacks.OrderBy(s => s.StackId).TakeWhile(s => s.Items <= 0).Count();
            game.Stacks.OrderBy(s => s.StackId).ElementAt(index).Items -= 1;
            this.MoveLogger.Information("{gameId}: DumbMove removed {numberToRemove} items from stack {whichStack}. Game after move: {game}.", game.GameId, 1, index + 1, game);
        }

        private void MakeBestMove(Game game, bool cheatMode = false)
        {
            var stacks = game.Stacks.OrderBy(s => s.StackId);

            var count_non_0_1 = stacks.Where(s => s.Items > 1).Count();
            var is_near_endgame = (count_non_0_1 <= 1);

            // nim sum will give the correct end-game move for normal play but
            // misere requires the last move be forced onto the opponent
            if (is_near_endgame)
            {
                var moves_left = stacks.Where(s => s.Items > 0).Count();
                var is_odd = (moves_left % 2 == 1);
                var sizeof_max = stacks.Max(s => s.Items);
                var maxStack = stacks.First(s => s.Items == sizeof_max);

                if (sizeof_max == 1 && is_odd)
                {
                    this.MoveLogger.Information("{gameId}: BestMove reverts to dumb move because max = 1 and is odd..", game.GameId);
                    this.MakeDumbMove(game);
                    return;
                }

                // reduce the game to an odd number of 1's
                maxStack.Items -= sizeof_max - (is_odd ? 1 : 0);
                return;
            }

            var nim_sum = stacks.Aggregate(0, (acc, curr) => acc ^ curr.Items);
            this.MoveLogger.Information("{gameId}: BestMove calculates num_sum to {nimSum} for game {game}.", game.GameId, nim_sum, game);

            if (nim_sum == 0)
            {
                this.MoveLogger.Information("{gameId}: BestMove reverts to dumb move because nim sum is 0.", game.GameId);
                this.MakeDumbMove(game);
                if(!cheatMode) // Only return here if we're not cheating. Otherwise, MakeDumbMove removes something and we make the best move possible after that.
                    return;

                this.MoveLogger.Information("{gameId}: Cheating BestMove calculates new num_sum to {nimSum} for game {game}.", game.GameId);
                nim_sum = stacks.Aggregate(0, (acc, curr) => acc ^ curr.Items);
            }

            // Calc which move to make
            for (int i = 0; i < stacks.Count(); i++)
            {
                var stack = stacks.ElementAt(i);
                var target_size = stack.Items ^ nim_sum;
                if (target_size < stack.Items)
                {
                    var amount_to_remove = stack.Items - target_size;

                    stack.Items -= amount_to_remove;

                    this.MoveLogger.Information("{gameId}: BestMove removed {numberToRemove} items from stack {whichStack}. Game after move: {game}.", game.GameId, amount_to_remove, i + 1, game);
                    return;
                }
            }

            this.MoveLogger.Information("{gameId}: BestMove didn't make a move, because reason.", game.GameId);
        }

        private void ComputerMakeMove(Player player, Game game)
        {
            if (player.Level < 2)
            {
                this.MakeDumbMove(game);
                return;
            }

            if (player.Level < 80)
            {
                this.MakeBestMove(game);
                return;
            }

            if (player.Level >= 127)
            {
                this.MakeBestMove(game, player.Level >= 130);
                return;
            }

            this.MakeDumbMove(game);
        }
    }
}
