using System;
using System.Globalization;
using System.Collections.Generic;

namespace Server.Controllers
{
    internal class ScoreComparer : IComparer<string>
    {
        public static bool IsNumeric(string value)
        {
            return int.TryParse(value, out _);
        }

        public int Compare(string s1, string s2)
        {
            const int S1GreaterThanS2 = 1;
            const int S2GreaterThanS1 = -1;

            if(string.IsNullOrEmpty(s1))
                if(string.IsNullOrEmpty(s2))
                    return 0;
                else
                    return S2GreaterThanS1; 

            if(string.IsNullOrEmpty(s2))
                if(string.IsNullOrEmpty(s1))
                    return 0;
                else
                    return S1GreaterThanS2;

            var IsNumeric1 = IsNumeric(s1);
            var IsNumeric2 = IsNumeric(s2);

            if (IsNumeric1 && IsNumeric2)
            {
                var i1 = Convert.ToInt32(s1);
                var i2 = Convert.ToInt32(s2);

                if (i1 > i2)
                {
                    return S1GreaterThanS2;
                }

                if (i1 < i2)
                {
                    return S2GreaterThanS1;
                }

                return 0;
            }

            return string.Compare(s1, s2, true, CultureInfo.InvariantCulture);
        }
    }
}
