using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Server.WebData
{
    /// <summary>
    /// The high score list.
    /// </summary>
    public class HighscoreList
    {
        /// <summary>
        /// A hihg score.
        /// </summary>
        public class Highscore
        {
            public string Alias { get; set; }
            public string Score { get; set; }
        }

        /// <summary>
        /// List of all high scores. Sorted by score.
        /// </summary>
        /// <value></value>
        public List<Highscore> Highscores { get; set; }
    }
}
