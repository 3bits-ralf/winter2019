using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Server.WebData
{
    /// <summary>
    /// *** DO NOT USE ***
    /// </summary>
    public class DebugScore
    {
        public string PlayerToken { get; set; }
        public string Score { get; set; }
        public string SecurityHashCode { get; set; }
    }
}
