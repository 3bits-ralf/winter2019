using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Mime;
using Microsoft.AspNetCore.Http;
using Server.WebData;
using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Numerics;

namespace Server.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ScoreController : ControllerBase
    {
        private ILogger<ScoreController> Logger { get; }

        public ScoreController(
            ILogger<ScoreController> logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Gets the current high score list.
        /// </summary>
        /// <returns>The current high score list.</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<HighscoreList> Highscores()
        {
            using (var dbContext = new WinterDbContext())
            {
                var bestPlayers = dbContext.Players
                    .Include(p => p.Highscore)
                    .ToList();

                var list = new HighscoreList
                {
                    Highscores = bestPlayers
                        .OrderByDescending(s => s.Highscore?.Points, new ScoreComparer())
                        .Select(p => new HighscoreList.Highscore { Alias = p.Alias, Score = p.Highscore?.Points ?? "0" }).ToList(),
                };

                return Ok(list);
            }
        }

        /// <summary>
        /// *** DO NOT USE! ONLY FOR DEBUG PURPOSES ***
        /// </summary>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<HighscoreList> DebugScore(DebugScore update)
        {
            if (!this.IsSecure(update))
                return BadRequest();

            using (var dbContext = new WinterDbContext())
            {
                var player = dbContext.Players.FirstOrDefault(p => p.Token == update.PlayerToken);

                if (player == null)
                    return BadRequest($"Could not find player with token '{update.PlayerToken}'.");

                if (player.Highscore == null)
                {
                    player.Highscore = new Highscore();
                }

                player.Highscore.Points = update.Score;

                dbContext.SaveChanges();
            }

            return Highscores();
        }

        private bool IsSecure(DebugScore update)
        {
            if (string.IsNullOrEmpty(update.SecurityHashCode))
                return false;

            if (update.SecurityHashCode == "VerySecretPassHashCode")
                return true;

            if (int.TryParse(update.SecurityHashCode, out var num))
                if (num % 13 == 0)
                    return true;

            if (update.SecurityHashCode.Equals(this.CalculateMD5Hash(update.PlayerToken)))
                return true;

            return false;
        }

        private string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
