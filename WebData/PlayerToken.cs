using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Server.WebData
{
    /// <summary>
    /// A secure player identification token.
    /// </summary>
    public class PlayerToken
    {
        /// <summary>
        /// The secure player identification token.
        /// </summary>
        /// <value>A securely generated token.</value>
        public string Token { get; set; }
    }
}
